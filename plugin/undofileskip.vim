"
" undofileskip.vim: Don't save undo history for temporary or secure files.
"
" Author: Tom Ryder <tom@sanctum.geek.nz>
" License: Same as Vim itself
"
if exists('loaded_undofileskip') || &compatible
  finish
endif
if !has('persistent_undo') || !exists('*glob2regpat')
  finish
endif
let loaded_undofileskip = 1

" Set default list of patterns to exclude; mirror documented 'backupskip'
" behavior
if !exists('g:undofileskip')
  let g:undofileskip = []
  if has('mac')
    call add(g:undofileskip, '/private/tmp/*')
  elseif has('unix')
    call add(g:undofileskip, '/tmp/*')
  endif
  call extend(g:undofileskip, map(
        \ filter([$TMPDIR, $TMP, $TEMP], 'v:val !=# '''''),
        \ 'v:val.''/*'''
        \))
endif

" Check the path on every buffer rename, create, or read
augroup undofileskip
  autocmd!
  autocmd BufFilePost,BufNewFile,BufRead *
        \ let &l:undofile = undofileskip#Check(expand('<amatch>'))
augroup END
