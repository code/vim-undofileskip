undofileskip.vim
================

This plugin emulates the `'backupskip'` option's functionality for the
`+persistent_undo` feature, checking buffer file paths against a list of globs,
and switching the `'undofile'` option off locally if any of them match.

License
-------

Copyright (c) [Tom Ryder][1].  Distributed under the same terms as Vim itself.
See `:help license`.

[1]: https://sanctum.geek.nz/
