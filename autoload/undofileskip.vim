" Internal function returns a local value for 'undofile'
function! undofileskip#Check(path) abort

  " If this isn't a normal buffer, don't save undo data
  if &buftype !=# ''
    return 0
  endif

  " Get the path from the buffer name; if that path matches any of the
  " patterns, don't save undo data
  for glob in g:undofileskip
    if a:path =~# glob2regpat(glob)
      return 0
    endif
  endfor

  " Otherwise, we'll use whatever the global setting is
  return &g:undofile

endfunction
